#!/bin/bash

set -e
clear

# APP ARGUMENTS
#
outcomeLang="$1"
workingDir="$2"
#
# APP RUNNING CONFIGS
#
javaHome="/C/java/openjdk-21.0.1"
mvnHome="/C/apache-maven-3.9.6"
fileName="garmin-translator-1.0-SNAPSHOT-runnable.jar"
#
#
#

encoding=65001
homeDir=$(pwd)
rootDir="$(cd .. && pwd)"

export PATH=${PATH}:$javaHome/bin/java
export PATH=${PATH}:$mvnHome/bin/mvn

title() {
  clear
  echo ""
  echo "*** $1 ***"
  echo ""
}

compile() {
  title "COMPILING PROJECT"
  echo "Root dir: $rootDir"
  echo ""
  cd "$rootDir"
  mvn clean
  sleep 1
  mvn install
  sleep 1
  cp "./target/$fileName" "$homeDir"
  cd "$homeDir"
}

validate() {
  title "RUNNING VALIDATIONS"
  if [[ ! -f "$fileName" ]]; then
    echo "ERROR: file not found - $fileName"
    echo ""
    read -n 1 -s -r -p "Press any key to compile the project... "
    echo ""
    compile
    echo ""
    if [[ ! -f "$fileName" ]]; then
      echo "ERROR: file not found even after compilation - $fileName"
      exit 1
    fi
  fi
}

encoding() {
  title "APPLYING ENCODING"
  if [[ "$OSTYPE" == "msys" || "$OSTYPE" == "cygwin" ]]; then
    chcp.com "$encoding"
  elif [[ "$OSTYPE" == "win32" ]]; then
    chcp.exe "$encoding"
  else
    echo "WARN: Opss, I could not set encoding for you!"
    echo "INFO: Please make sure the encoding is \"UTF-8\" before continuing."
    echo ""
    read -n 1 -s -r -p "Press any key to continue..."
  fi
}

validate
encoding

java -jar "$homeDir/$fileName" "outcomeLang=$outcomeLang" "workingDir=$workingDir"
