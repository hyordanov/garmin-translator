# Garmin Translator

# Requisites

* JDK 21
* Maven
* cURL

# Compile & Run

* [start-garmin-translator.sh](./bin/start-garmin-translator.sh)

# Features

Type each of the commands below to take its action:

* :LANG_NAME - to copy the message directly from that language (e.g., :br or :bg);

**NOTE:** the features above do not work for Headers.

# Author

hyordanov
