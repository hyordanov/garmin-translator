package pt.hristiyan.garmin.utils;

import org.junit.jupiter.api.Test;

class ScannerUtilsTest {

    @Test
    void printPercentageTest() {
        ScannerUtils.print("%1% any\n");
        ScannerUtils.print("%s", "%1 % any");
    }
}
