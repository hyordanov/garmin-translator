package pt.hristiyan.garmin.language;

import lombok.Getter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public class Language {

    public static boolean ONLINE_TRANSLATION_YES = true;
    public static boolean ONLINE_TRANSLATION_NOO = false;

    private final File file;
    private final String name;
    private final List<LanguageItem> items;
    private final boolean onlineTranslation;

    private String header;

    public Language(String name, boolean onlineTranslation, File file) {
        this.name = name;
        this.file = file;
        this.items = new ArrayList<>();
        this.onlineTranslation = onlineTranslation;

        if (this.file.isFile()) {
            mapData();
        }
    }

    private void mapData() {
        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {

            header = br.readLine(); // always the first line

            String line;
            while ((line = br.readLine()) != null) {
                LanguageItem item = new LanguageItem(line, this);
                items.add(item);
            }

        } catch (Exception e) {
            throw new RuntimeException("Language(mapData): failed to parse file - " + file, e);
        }
    }

    public LanguageItem findItem(String key) {
        Optional<LanguageItem> result = this.items.parallelStream()
                .filter(item -> item.getKey().equals(key))
                .findFirst();
        return result.orElse(null);
    }

    public String getHeader() {
        return header != null ? header : "<null>";
    }
}
