package pt.hristiyan.garmin.language;

import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class LanguageItem {

    private static final Pattern pattern = Pattern.compile("(^\\S{8}) (.*)$");

    private final String key;
    private final String value;
    private final Language parent;

    public LanguageItem(String data, Language parent) {
        Matcher matcher = pattern.matcher(data);
        if (!matcher.find()) {
            throw new RuntimeException("LanguageItem(init): failed to parse data - " + data);
        }
        this.parent = parent;
        this.key = matcher.group(1);
        this.value = matcher.group(2);
    }
}
