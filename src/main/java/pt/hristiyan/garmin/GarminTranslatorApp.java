package pt.hristiyan.garmin;

import pt.hristiyan.garmin.language.Language;
import pt.hristiyan.garmin.translators.GarminTranslator;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static pt.hristiyan.garmin.language.Language.ONLINE_TRANSLATION_NOO;
import static pt.hristiyan.garmin.language.Language.ONLINE_TRANSLATION_YES;
import static pt.hristiyan.garmin.utils.FileUtils.isDirectory;
import static pt.hristiyan.garmin.utils.ScannerUtils.*;

public class GarminTranslatorApp {

    private static final String OUTCOME_FILE_NAME = "outcome.ln4";

    public static void main(String[] args) {
        addShutdownHook();

        String outcomeLanguage = setupOutcomeLanguage(args);
        String workingDir = setupWorkingDir(args);
        File[] files = loadFiles(workingDir);
        Language[] languages = parseFiles(files);
        Language[] baselines = setupBaselines(languages);

        // translation config
        File file = new File(workingDir, OUTCOME_FILE_NAME);
        GarminTranslator garminTranslator = new GarminTranslator(outcomeLanguage, file);

        clear();
        println("%nInitializing translation to %s...", outcomeLanguage);
        println("%nTo file - %s", file.getAbsoluteFile());
        sleep(1);
        garminTranslator.translate(baselines);
        println("%nTranslation to %s: done", outcomeLanguage);
        sleep(1);
    }

    private static void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            println("Shutting down...");
            // do something if necessary
            print("%nGarmin translator terminated successfully! ");
            pressEnterToContinue();
        }));

        print("%nGarmin translator started successfully! ");
        sleep(1);
    }

    private static String setupOutcomeLanguage(String[] args) {
        clear();
        println("%nSetting up outcome language...%n");
        String result = null;

        final String regex = "^outcomeLang=(.*)$";
        Optional<String> directory = Arrays.stream(args).filter(p -> p.matches(regex)).findFirst();
        if (directory.isPresent()) {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(directory.get());
            if (m.find()) {
                result = m.group(1);
                if (result.length() != 2) {
                    println("ERROR: outcome language not valid - %s", result);
                    sleep(1);
                    result = null; // not a valid outcome language
                } else {
                    println("Outcome language: %s", result);
                    sleep(1);
                }
            }
        }

        if (result == null) {
            println("%nTIP: outcome language must have only two characters (e.g., \"pt\", \"bg\", etc.).%n");
            while (true) {
                result = readln("Type outcome language: ");
                if (result.length() == 2) {
                    break;
                }
                println("ERROR: outcome language not valid - %s%n", result);
            }
        }

        return result;
    }

    private static String setupWorkingDir(String[] args) {
        clear();
        println("%nSetting up the working directory...%n");
        String result = null;

        final String regex = "^workingDir=(.*)$";
        Optional<String> directory = Arrays.stream(args).filter(p -> p.matches(regex)).findFirst();
        if (directory.isPresent()) {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(directory.get());
            if (m.find()) {
                result = m.group(1);
                if (!isDirectory(result)) {
                    println("ERROR: directory not valid - %s%n", result);
                    sleep(1);
                    result = null; // not a valid directory
                } else {
                    println("Working directory: %s", result);
                    sleep(1);
                }
            }
        }

        if (result == null) {
            while (true) {
                result = readln("Type directory: ");
                if (isDirectory(result)) {
                    break;
                }
                println("ERROR: directory not valid - %s%n", result);
            }
        }

        return result;
    }

    private static File[] loadFiles(String workingDir) {
        clear();
        println("%nLoading language files (*.ln4)...%n");

        File[] result = new File(workingDir).listFiles((dir, name) -> !name.matches(OUTCOME_FILE_NAME) && name.matches(".*(\\.ln4$)"));
        int length = result != null ? result.length : 0;
        println("Language files: %s", length);
        if (length == 0) {
            throw new RuntimeException("GarminTranslatorApp(loadFiles): at least 1 language file is required");
        }

        sleep(1);
        return result;
    }

    private static Language[] parseFiles(File[] files) {
        clear();
        println("%nParsing language files...");
        println("%n*** TIP ***");
        println("Filename mask allowed: #{1}-#{2}.ln4");
        println(" #{1} = the language name (e.g., de, pt, bg, etc.)");
        println(" #{2} = enable duckduck online translation - yes or no");
        print("You will be prompted for each file if the mask is not identified.");
        pressEnterToContinue();

        BiFunction<String, Boolean, String> validateLanguageName = (name, printIfValid) -> {
            boolean valid = name != null && name.length() == 2;
            if (valid) {
                if (printIfValid) {
                    println(" Language name: %s", name);
                }
            } else {
                println(" ERROR: language name not valid - %s", name);
            }
            return valid ? name : "";
        };

        BiFunction<String, Boolean, String> validateTranslate = (translate, printIfValid) -> {
            boolean valid = "yes".equalsIgnoreCase(translate) || "no".equalsIgnoreCase(translate) ||
                    "y".equalsIgnoreCase(translate) || "n".equalsIgnoreCase(translate);
            if (valid) {
                if (printIfValid) {
                    println(" Online translation: %s", translate);
                }
            } else {
                println(" ERROR: online translation not valid - %s", translate);
            }
            return valid ? translate : "";
        };

        Language[] result = new Language[files.length];

        final String regex = "(.*)-(.*)\\.ln4$";
        File file;
        String name, lang, translate;
        for (int i = 0; i < files.length; i++) {
            file = files[i];
            name = file.getName();
            lang = "";
            translate = "";
            println("%nParsing: %s...", name);

            boolean hasMask = name.matches(regex);
            if (hasMask) {
                Pattern p = Pattern.compile(regex);
                Matcher m = p.matcher(name);
                if (m.find()) {
                    lang = validateLanguageName.apply(m.group(1), true);
                    translate = validateTranslate.apply(m.group(2), true);
                }
            }

            while (lang.isEmpty()) {
                lang = validateLanguageName.apply(readln(" Type language name: "), false);
            }
            while (translate.isEmpty()) {
                translate = validateTranslate.apply(readln(" Enable online translation? [y/n]: "), false);
            }

            boolean onlineTranslation = translate.toLowerCase().startsWith("y") ? ONLINE_TRANSLATION_YES : ONLINE_TRANSLATION_NOO;
            result[i] = new Language(lang, onlineTranslation, file);
        }

        println("%nParsed language files: %s", result.length);
        sleep(3); // more time to see which files were parsed

        return result;
    }

    private static Language[] setupBaselines(Language[] languages) {
        clear();
        println("%nSetting up baseline languages...%n");
        if (languages.length > 1) {
            String delimiter = ", ";
            String names = Arrays.stream(languages).map(Language::getName).collect(Collectors.joining(delimiter));
            println("More than one language detected!%n%nWhich one do you want to use as a baseline?: %s%n", names);
            List<String> namesList = Arrays.stream(names.split(delimiter)).toList();
            String input;
            while (true) {
                input = readln("Type a baseline language: ");
                if (namesList.contains(input)) {
                    break;
                }
                println("ERROR: baseline language not valid - %s%n", input);
            }

            final String userBaseline = input;
            Optional<Language> baseline = Arrays.stream(languages).filter((l) -> l.getName().equals(userBaseline)).findFirst();
            if (baseline.isEmpty()) {
                throw new RuntimeException("GarminTranslatorApp(setupBaselines): language not found - " + userBaseline);
            }

            Language[] tmp = new Language[languages.length];
            tmp[0] = baseline.get();
            int counter = 1;
            for (Language l : languages) {
                if (!userBaseline.equals(l.getName())) {
                    tmp[counter++] = l;
                }
            }

            languages = tmp;
            println("");
        }

        println("Baseline language: %s", languages[0].getName());
        sleep(1);
        return languages;
    }
}
