package pt.hristiyan.garmin.translators;

import org.json.JSONObject;
import pt.hristiyan.garmin.utils.WebUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DuckDuckTranslator {

    private static DuckDuckTranslator instance;

    private final String query = "traduzir";
    private final String vqd;

    private DuckDuckTranslator() {
        String home = WebUtils.makeGet("https://duckduckgo.com/?q=" + query + "&ia=web");

        Pattern pattern = Pattern.compile("vqd=(\\d-\\d+)");
        Matcher matcher = pattern.matcher(home);
        if (!matcher.find()) {
            throw new RuntimeException("DuckDuckTranslator(init): failed to detect VQD identifier - " + home);
        }
        this.vqd = matcher.group(1);
    }

    public static DuckDuckTranslator getInstance() {
        if (instance == null) {
            instance = new DuckDuckTranslator();
        }
        return instance;
    }

    public String translate(String text, String from, String to) {
        String url = "https://duckduckgo.com/translation.js"
                + "?vqd=" + vqd
                + "&query=" + query
                + "&from=" + from
                + "&to=" + to;

        String data = WebUtils.makeGet(url, text);
        JSONObject result = new JSONObject(data);
        String key = "translated";
        if (!result.has(key)) {
            throw new RuntimeException("DuckDuckTranslator(translate): json key \"" + key + "\" not found - " + data);
        }

        return result.getString(key);
    }
}
