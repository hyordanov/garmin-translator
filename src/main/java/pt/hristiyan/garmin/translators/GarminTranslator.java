package pt.hristiyan.garmin.translators;

import pt.hristiyan.garmin.language.Language;
import pt.hristiyan.garmin.language.LanguageItem;
import pt.hristiyan.garmin.utils.DateUtils;
import pt.hristiyan.garmin.utils.FileUtils;
import pt.hristiyan.garmin.utils.ScannerUtils;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class GarminTranslator {

    // metadata index
    public static final long MD_PROGRESS = 0;
    public static final long MD_CREATE_TIME = 1;
    public static final long MD_UPDATE_TIME = 2;
    public static final long MD_END_TIME = 3;

    private final String language;
    private final File outcome;
    private final File metadata; // already processed items

    public GarminTranslator(String language, File outcome) {
        if (!outcome.isFile()) {
            FileUtils.create(outcome, "GarminTranslator(init): failed to create new language file");
        }

        String directory = outcome.getParent();
        String filename = String.format("%s.metadata", outcome.getName());
        this.metadata = new File(directory, filename);
        if (!metadata.isFile()) {
            FileUtils.create(metadata, "GarminTranslator(init): failed to create new metadata file");
            String error = "GarminTranslator(init): failed to write %s to metadata file";
            FileUtils.write(metadata, MD_PROGRESS, "0", String.format(error, "progress"));
            FileUtils.write(metadata, MD_CREATE_TIME, DateUtils.instant(), String.format(error, "create_time"));
        }

        this.language = language;
        this.outcome = outcome;
    }

    private int getProgress() {
        String error = "GarminTranslator(getProgress): failed to read current progress value from file";
        String line = FileUtils.read(metadata, MD_PROGRESS, error);
        return Integer.parseInt(line);
    }

    private int updateProgress(int current) {
        current += 1;
        String error = "GarminTranslator(updateProgress): failed to update %s on the metadata file";
        FileUtils.write(metadata, MD_PROGRESS, String.valueOf(current), String.format(error, "progress"));
        FileUtils.write(metadata, MD_UPDATE_TIME, DateUtils.instant(), String.format(error, "update_time"));
        return current;
    }

    private int saveOutcome(String type, String line, int progress) {
        String error = "GarminTranslator(writeLine): failed to write %s to the language file";
        FileUtils.write(outcome, progress, line, String.format(error, type));
        progress = updateProgress(progress);
        ScannerUtils.println("%nSuccess: %s", type);
        ScannerUtils.sleep(1);
        return progress;
    }

    private void showDetails(LanguageItem item, Language... baselines) {
        final String key = item.getKey();

        // metadata
        LinkedHashMap<String, LanguageItem> items = new LinkedHashMap<>();
        String value, name, output;
        int maxLength = 0;

        // build secondary items
        LanguageItem li;
        for (Language language : baselines) {
            li = language.findItem(key);
            value = li != null ? li.getValue() : "<null>";
            name = language.getName();
            output = String.format("  %s | %s | %s", name, key, value);
            maxLength = Math.max(output.length(), maxLength);
            items.put(output, li);
        }

        // build main item
        output = String.format("* %s | %s | %s", item.getParent().getName(), key, item.getValue());
        maxLength = Math.max(output.length(), maxLength);
        items.put(output, item);

        // show items by:
        //  1 - right pad spaces
        //  2 - translate item value if online translator enabled
        final int fixedLength = maxLength;
        for (Map.Entry<String, LanguageItem> entry : items.entrySet()) {
            String line = entry.getKey();
            LanguageItem owner = entry.getValue();

            String spaces = " ".repeat(Math.max(0, fixedLength - line.length()));
            ScannerUtils.print("%s%s | ", line, spaces);

            Language parent = owner.getParent();
            if (parent.isOnlineTranslation()) {
                DuckDuckTranslator translator = DuckDuckTranslator.getInstance();
                String translated = translator.translate(owner.getValue(), parent.getName(), this.language);
                ScannerUtils.print(translated);
            }
            ScannerUtils.println("");
        }
    }

    private String processMessage(String message, LanguageItem item, Language... languages) {
        boolean isCopyFrom = message.matches("^(:\\S+).*");
        if (isCopyFrom) {
            String key = item.getKey();
            String name = message.substring(1);

            Optional<Language> language = Arrays.stream(languages)
                    .filter(l -> l.getName().startsWith(name))
                    .findFirst();
            if (language.isEmpty()) {
                ScannerUtils.println("ERROR: language not found - %s%n", name);
                String messageNew = ScannerUtils.readln("  %s | %s | ", this.language, key);
                return processMessage(messageNew, item, languages);
            }

            LanguageItem languageItem = language.get().findItem(key);
            if (languageItem == null) {
                ScannerUtils.println("ERROR: key not found - %s%n", name);
                String messageNew = ScannerUtils.readln("  %s | %s | ", this.language, key);
                return processMessage(messageNew, item, languages);
            }
            message = languageItem.getValue();
            ScannerUtils.println("%nINFO: copied from %s - \"%s\" ", languageItem.getParent().getName(), message);
            ScannerUtils.sleep(1);
        }

        return message;
    }

    public void translate(Language... baselines) {

        // validate baselines and also set primary and secondaries
        if (baselines == null || baselines.length == 0) {
            throw new RuntimeException("GarminTranslator(translate): baseline language is missing");
        }
        final Language primary = baselines[0];
        final Language[] secondaries;
        if (baselines.length > 1) {
            secondaries = Arrays.copyOfRange(baselines, 1, baselines.length);
        } else {
            secondaries = new Language[0];
        }

        // load progress value so we can restart translation later
        int progress = getProgress();
        final int progressMax = primary.getItems().size();

        // translate header
        if (progress == 0) {
            ScannerUtils.clear();
            ScannerUtils.println("%n********** HEADER **********%n");
            for (Language s : secondaries) {
                ScannerUtils.print("  %s | %s%n", s.getName(), s.getHeader());
            }
            ScannerUtils.print("* %s | %s%n", primary.getName(), primary.getHeader());

            // ours
            String header = ScannerUtils.readln("  %s | ", this.language);
            progress = saveOutcome("header", header, progress);
        }

        // translate baseline
        for (int i = progress - 1; i < progressMax - 1; i++) {
            ScannerUtils.clear();
            ScannerUtils.println("%n****************************** LANGUAGE ITEM ******************************%n");
            LanguageItem item = primary.getItems().get(i);
            String key = item.getKey();

            // a little info
            ScannerUtils.print("Processing (%s/%s): %s%n%n", progress + 1, progressMax, key);
            showDetails(item, secondaries);

            // ours
            String message = ScannerUtils.readln("%n  %s | %s | ", this.language, item.getKey());
            message = processMessage(message, item, baselines);
            progress = saveOutcome(key, key + " " + message, progress);
            ScannerUtils.println("");
        }

        String error = "GarminTranslator(translate): failed to write end_time to metadata file";
        FileUtils.write(metadata, MD_END_TIME, DateUtils.instant(), error);
    }
}
