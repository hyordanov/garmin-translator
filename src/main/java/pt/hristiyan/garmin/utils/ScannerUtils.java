package pt.hristiyan.garmin.utils;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ScannerUtils {

    private static final Scanner IN = new Scanner(System.in);

    private ScannerUtils() {
    }

    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException ignored) {
        }
    }

    public static void clear() {
        try {
            print("\033[H\033[2J");
        } catch (Exception ignored) {
        }
    }

    public static void pressEnterToContinue() {
        try {
            print("%n%nPress enter to continue... ");
            String ignored = IN.nextLine();
        } catch (Exception ignored) {
        }
    }

    public static void print(String message, Object... args) {
        try {
            boolean hasArgs = message.contains("%s");
            boolean hasNl = message.contains("%n");
            if (!hasArgs && !hasNl) {
                message = message.replace("%", "%%");
            }

            PrintStream out = new PrintStream(System.out, true, StandardCharsets.UTF_8);
            out.printf(message, args);
        } catch (Exception e) {
            throw new RuntimeException("ScannerUtils(print): failed to print message - " + message, e);
        }
    }

    public static void println(String message, Object... args) {
        print(message + "%n", args);
    }

    public static String readln(String message, Object... args) {
        String result = null;

        try {
            print(message, args);

            while ((result = IN.nextLine()) != null) {
                if (!result.trim().isEmpty()) {
                    break;
                }
                println("ERROR: value must not be empty. Please try again...");
                if (!message.startsWith("%n")) {
                    println("");
                }
                print(message, args);
            }
        } catch (Exception ignored) {
            // re-try if any exception
            sleep(3);
            readln(message, args);
        }

        return result;
    }
}
