package pt.hristiyan.garmin.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    private DateUtils() {
    }

    public static String instant() {
        return SDF.format(new Date());
    }
}
