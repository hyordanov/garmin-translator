package pt.hristiyan.garmin.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class WebUtils {

    private WebUtils() {
    }

    private static Process execute(String... command) {
        try {
            ProcessBuilder builder = new ProcessBuilder(command);
            return builder.start();
        } catch (Exception e) {
            throw new RuntimeException("WebUtils(execute): failed to start process - " + Arrays.toString(command), e);
        }
    }

    private static String read(Process process) {
        try (InputStream is = process.getInputStream();
             InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (Exception e) {
            throw new RuntimeException("WebUtils(read): failed to read process data - process exit code " + process.exitValue() + " (0=success)", e);
        } finally {
            process.destroy();
            if (process.isAlive()) {
                process.destroyForcibly();
            }
        }
    }

    public static String makeGet(String url) {
        Process process = execute("curl", "-X", "GET", url);
        return read(process);
    }

    public static String makeGet(String url, String raw) {
        Process process = execute("curl", "-X", "GET", url, "--data-raw", "\"" + raw + "\"");
        return read(process);
    }
}
