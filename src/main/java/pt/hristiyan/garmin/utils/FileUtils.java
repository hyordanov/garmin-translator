package pt.hristiyan.garmin.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

public class FileUtils {

    private static final String lineSeparator = "\n";

    private FileUtils() {
    }

    public static boolean isDirectory(String path) {
        boolean result = false;

        if (path != null) {
            File directory = new File(path);
            if (directory.isDirectory()) {
                result = true;
            }
        }

        return result;
    }

    public static void create(File file, String error) {
        error += " - " + file; // add file path to the error message
        try {
            if (!file.createNewFile()) {
                throw new RuntimeException(error);
            }
        } catch (Exception e) {
            throw new RuntimeException(error, e);
        }
    }

    public static String read(File file, long index, String error) {
        error += " - " + file; // add file path to the error message

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {

            long myIndex = 0;
            String line;
            while ((line = br.readLine()) != null) {
                if (++myIndex >= index) {
                    break;
                }
            }

            return line;
        } catch (Exception e) {
            throw new RuntimeException(error, e);
        }
    }

    public static void write(File file, long index, String value, String error) {
        error += " - " + file; // add file path to the error message
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            long current = 0, seek = 0;
            while (++current <= index) {
                String line = raf.readLine();
                if (line != null) {
                    seek += line.length() + 1;
                }
            }
            raf.seek(seek);
            raf.write((value + lineSeparator).getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new RuntimeException(error, e);
        }
    }
}
